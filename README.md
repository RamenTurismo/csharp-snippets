# README

C# Snippets are stored in `Documents` > `Visual Studio 20xx` > `Code Snippets`.

To open up the snippets manager, press `CTRL+K,CTRL+B` on Visual Studio, and add the folders if necessary.

[Walkthrough: Create a code snippet](https://docs.microsoft.com/en-us/visualstudio/ide/walkthrough-creating-a-code-snippet?view=vs-2022)